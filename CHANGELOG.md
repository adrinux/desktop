# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

All releases can be found on https://code.vikunja.io/desktop/releases.

The releases aim at the api and frontend versions which is why there are missing versions.

## [0.16.0 - 2021-01-10]

For a list of changes in this release, see [the frontend changelog](https://kolaente.dev/vikunja/frontend/releases/tag/v0.16.0).

### Added

* Add yarn cache to drone
* Configure Renovate (#1)

### Changed

* Change license to GPLv3
* Pin dependencies (#2)
* Update dependency electron to v10.1.5 (#3)
* Update dependency electron to v11.0.1 (#5)
* Update dependency electron to v11.0.2 (#6)
* Update dependency electron to v11.0.3 (#7)
* Update dependency electron to v11.0.4 (#8)
* Update dependency electron to v11.1.0 (#9)
* Update dependency electron to v11.1.1 (#10)
* Update dependency electron to v11 (#4)

## [0.15.0 - 2020-10-19]

First initial release.

For a list of changes in this release, see [the frontend changelog](https://kolaente.dev/vikunja/frontend/releases/tag/v0.15.0).

